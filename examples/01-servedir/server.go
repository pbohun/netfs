package main

import (
	"log"
	"os"
	"fmt"
	"gitlab.com/pbohun/netfs"
	"gitlab.com/pbohun/troposphere/security"
	"crypto/rsa"
)

var testdir string = "../../examples/testfiles"

func main() {
	privKey, err := getKey()
	if err != nil {
		fmt.Println("Could not create keypair:", err)
		return
	}
	server := netfs.NewServer()
	server.AddService("/", netfs.NewDirService(testdir, privKey, 8192))
	server.ListenAndServe(":8888")
}

func getKey() (*rsa.PrivateKey, error) {
	fileContents, err := os.ReadFile(testdir + "/serverkey.pem")
	if err != nil {
		key, err := security.GenerateKeypair()
		if err != nil {
			log.Fatal("Could not generate rsa keys:", err)
		}
		s := security.ExportPrivateKey(key)
		os.WriteFile(testdir+"/serverkey.pem", []byte(s), 0777)
		return key, nil
	}
	return security.ImportPrivateKey(string(fileContents))
}

