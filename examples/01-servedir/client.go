package main

import (
	"log"
	"os"
	"crypto/rsa"
	"fmt"
	"gitlab.com/pbohun/netfs"
	"gitlab.com/pbohun/troposphere/security"
)

var testdir string = "../../testfiles"

func main() {
	privKey, _ := getKey()
	fs := netfs.NewFs("1.1", privKey)

	if err := fs.Connect("http://127.0.0.1:8888"); err != nil {
		fmt.Println("Could not connect:", err)
		return
	}
	defer fs.Disconnect()

	// write
	if _, err := fs.Write("/test.txt", 0, []byte("hello there")); err != nil {
		fmt.Println("Could not write file:", err)
		return
	}
	fmt.Println("Write Success")

	// read
	b, err := fs.Read("/test.txt", 0, -1);
	if err != nil {
		fmt.Println("read error:", err)
		return
	}
	fmt.Println("Read:", string(b))
	fmt.Println("Read Success")

	// mkdir
	err = fs.Mkdir("/mydir")
	if err != nil {
		fmt.Println("mkdir error:", err)
		return
	}
	fmt.Println("mkdir success")

	// stat
	stat, err := fs.Stat("/mydir")
	if err != nil {
		fmt.Println("stat error:", err)
		return
	}
	fmt.Println("stat:", stat)
	fmt.Println("stat success")

	// remove
	if err = fs.Remove("/mydir"); err != nil {
		fmt.Println("remove error:", err)
		return
	}
	fmt.Println("remove success")

	// read directory
	flistbytes, err := fs.Read("/", 0, -1)
	if err != nil {
		fmt.Println("read dir error:", err)
		return
	}
	fmt.Println("files in dir:", string(flistbytes))
	fmt.Println("read dir success")

	fmt.Println("example 01 success!")
}

func getKey() (*rsa.PrivateKey, error) {
        fileContents, err := os.ReadFile(testdir + "/clientkey.pem")
        if err != nil {
                key, err := security.GenerateKeypair()
                if err != nil {
                        log.Fatal("Could not generate rsa keys:", err)
                }
                s := security.ExportPrivateKey(key)
                os.WriteFile(testdir+"/clientkey.pem", []byte(s), 0777)
                return key, nil
        }
        return security.ImportPrivateKey(string(fileContents))
}

