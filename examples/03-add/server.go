package main

import (
	"strconv"
	"errors"
	"encoding/json"
	"fmt"
	"gitlab.com/pbohun/netfs"
	"gitlab.com/pbohun/troposphere/security"
)

func main() {
        privKey, err := security.GenerateKeypair()
        if err != nil {
                fmt.Println("Could not generate keypair:", err)
                return
        }
        server := netfs.NewServer()
	service := netfs.NewService(privKey, 8192)
	service.Exec = func(path string, data []byte) ([]byte, error) {
		if path != "/add" {
			return []byte{}, errors.New(fmt.Sprintf("Invalid path:", path))
		}
		var req AddRequest
		if err := json.Unmarshal(data, &req); err != nil {
			return []byte{}, err
		}
		c := req.A + req.B
		return []byte(strconv.FormatInt(int64(c), 10)), nil
	}
	server.AddService("/", service)
	server.ListenAndServe(":8888")
}

type AddRequest struct {
	A int `json:"a"`
	B int `json:"b"`
}

