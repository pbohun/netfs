package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/pbohun/netfs"
	"gitlab.com/pbohun/troposphere/security"
)

func main() {
	privKey, err := security.GenerateKeypair()
	if err != nil {
		fmt.Println("Could not generate keypair:", err)
		return
	}
	fs := netfs.NewFs("1.1", privKey)
	err = fs.Connect("http://localhost:8888")
	if err != nil {
		fmt.Println("Could not connect to service:", err)
		return
	}
	req := AddRequest{A:3, B:4}
	res, err := fs.Exec("/add", req.Json())
	if err != nil {
		fmt.Println("Error adding numbers:", err)
		return
	}
	fmt.Println("result:", string(res))
	fmt.Println("success!")
}

type AddRequest struct {
	A int `json:"a"`
	B int `json:"b"`
}

func (a AddRequest) Json() []byte {
	b, _ := json.Marshal(a)
	return b
}

