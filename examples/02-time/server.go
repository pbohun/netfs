package main

import (
	"errors"
	"time"
	"fmt"
	"gitlab.com/pbohun/netfs"
	"gitlab.com/pbohun/troposphere/security"
)

func main() {
	privKey, err := security.GenerateKeypair()
	if err != nil {
		fmt.Println("Could not generate keypair:", err)
		return
	}
	server := netfs.NewServer()
	service := netfs.NewService(privKey, 8192)
	service.Read = func(path string, offset, length int64) ([]byte, error) {
		if path != "/time" {
			return []byte{}, errors.New(fmt.Sprintf("Invalid path:", path))
		}
		return []byte(time.Now().String()), nil
	}
	server.AddService("/", service)
	server.ListenAndServe(":8888")
}

