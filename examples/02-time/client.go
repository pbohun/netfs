package main

import (
	"fmt"
	"gitlab.com/pbohun/netfs"
	"gitlab.com/pbohun/troposphere/security"
)

func main() {
	privKey, err := security.GenerateKeypair()
	if err != nil {
		fmt.Println("Could not generate keypair:", err)
		return
	}
	fs := netfs.NewFs("1.1", privKey)
	err = fs.Connect("http://localhost:8888")
	if err != nil {
		fmt.Println("Could not connect to service:", err)
		return
	}
	timebytes, err := fs.Read("/time", 0, -1)
	if err != nil {
		fmt.Println("Could not read time:", err)
		return
	}
	fmt.Println("time is:", string(timebytes))
	fmt.Println("success!")
}

