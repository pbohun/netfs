package netfs

import (
	"bufio"
	"crypto/rsa"
	"fmt"
	"io"
	"net/http"

	_ "github.com/glebarez/go-sqlite"
	tro "gitlab.com/pbohun/troposphere"
)

const longestSession = 60 * 60 // session expires after an hour

type Service struct {
	privKey             *rsa.PrivateKey
	maxMessageSizeBytes int
	rootDir             string
	version             string

	Read   func(string, int64, int64) ([]byte, error)
	Write  func(string, int64, int64, []byte) (int64, error)
	Remove func(string) error
	Exec   func(string, []byte) ([]byte, error)
	Mkdir  func(string) error
	Stat   func(string) (tro.Stat, error)
}

type Account struct {
	Name            string
	Hash            string
	Groups          []string
	Created         int64
	LastInteraction int64
}

// NewService creates and returns a new server with the given private key.
func NewService(privKey *rsa.PrivateKey, maxMessageSizeBytes int) Service {
	s := Service{
		privKey:             privKey,
		maxMessageSizeBytes: maxMessageSizeBytes,
	}
	s.version = "1.1"
	if privKey == nil {
		s.version = "1.0"
	}
	return s
}

func (s *Service) SetRootDir(dir string) {
	c := dir[len(dir)-1]
	if c == '/' || c == '\\' {
		dir = dir[:len(dir)-1]
	}
	s.rootDir = dir
}

func (s *Service) Func() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		m, err := tro.ReadMessage(bufio.NewReader(r.Body), s.maxMessageSizeBytes)
		if err != nil {
			sendError(w, s.privKey, m.Path, "Error reading message.")
			return
		}

		path := s.rootDir + m.Path

		switch m.KindInt() {
		case tro.Tversion:
			resp, _ := tro.MkMessage(tro.Rversion, m.Path, s.privKey, 0, 0, []byte(s.version))
			io.WriteString(w, resp.String())
			return
		case tro.Tauth:
			sendError(w, s.privKey, m.Path, "Auth not available")
			return
		case tro.Tread:
			if s.Read == nil {
				sendError(w, s.privKey, m.Path, "Tread operation not available.")
				return
			}
			b, err := s.Read(path, m.Offset, m.Length)
			if err != nil {
				sendError(w, s.privKey, m.Path, err.Error())
				return
			}
			resp, _ := tro.MkMessage(tro.Rread, m.Path, s.privKey, 0, 0, b)
			io.WriteString(w, resp.String())
		case tro.Twrite:
			if s.Write == nil {
				sendError(w, s.privKey, m.Path, "Twrite operation not available.")
				return
			}
			b, err := s.Write(path, m.Offset, m.Length, m.Content)
			if err != nil {
				fmt.Println("twrite:", err)
				sendError(w, s.privKey, m.Path, err.Error())
				return
			}
			resp, _ := tro.MkMessage(tro.Rwrite, m.Path, s.privKey, 0, b, []byte{})
			io.WriteString(w, resp.String())
		case tro.Tremove:
			if s.Remove == nil {
				sendError(w, s.privKey, m.Path, "Tremove operation not available.")
				return
			}
			err := s.Remove(path)
			if err != nil {
				fmt.Println("tremove:", err)
				sendError(w, s.privKey, m.Path, err.Error())
				return
			}
			resp, _ := tro.MkMessage(tro.Rremove, m.Path, s.privKey, 0, 0, []byte{})
			io.WriteString(w, resp.String())
		case tro.Texec:
			if s.Exec == nil {
				sendError(w, s.privKey, m.Path, "Texec operation not available.")
				return
			}
			b, err := s.Exec(path, m.Content)
			if err != nil {
				fmt.Println("texec:", err)
				sendError(w, s.privKey, m.Path, err.Error())
				return
			}
			resp, _ := tro.MkMessage(tro.Rexec, m.Path, s.privKey, 0, 0, b)
			io.WriteString(w, resp.String())
		case tro.Tmkdir:
			if s.Mkdir == nil {
				sendError(w, s.privKey, m.Path, "Tmkdir operation not available.")
				return
			}
			err := s.Mkdir(path)
			if err != nil {
				sendError(w, s.privKey, m.Path, err.Error())
				return
			}
			resp, _ := tro.MkMessage(tro.Rmkdir, m.Path, s.privKey, 0, 0, []byte{})
			io.WriteString(w, resp.String())
		case tro.Tstat:
			if s.Stat == nil {
				sendError(w, s.privKey, m.Path, "Tstat operation not available.")
				return
			}
			stat, err := s.Stat(path)
			if err != nil {
				sendError(w, s.privKey, m.Path, err.Error())
				return
			}
			resp, _ := tro.MkMessage(tro.Rstat, m.Path, s.privKey, 0, 0, stat.Bytes())
			io.WriteString(w, resp.String())
		default:
			sendError(w, s.privKey, m.Path, "Invalid message kind:"+m.Kind)
		}
	}
}

func sendError(w http.ResponseWriter, privKey *rsa.PrivateKey, path, msg string) {
	resp, _ := tro.MkMessage(tro.Rerror, path, privKey, 0, 0, []byte(msg))
	io.WriteString(w, resp.String())
}
