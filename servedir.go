package netfs

import (
	"crypto/rsa"
	"errors"
	"fmt"
	tro "gitlab.com/pbohun/troposphere"
	"math"
	"os"
	"strconv"
	"strings"
)

func NewDirService(dir string, privKey *rsa.PrivateKey, maxMessageSizeBytes int) Service {
	service := NewService(privKey, maxMessageSizeBytes)

	service.SetRootDir(dir)

	service.Read = func(path string, offset, length int64) ([]byte, error) {
		info, err := os.Lstat(path)
		if err != nil {
			return []byte{}, errors.New("File/directory does not exist:" + path)
		}
		if info.IsDir() {
			maxEntries := length
			if maxEntries < 0 {
				maxEntries = math.MaxInt32 - offset
			}
			files, _ := os.ReadDir(path)
			if offset > int64(len(files)) {
				return []byte{}, nil
			}
			list := []string{}
			for i, f := range files[offset:] {
				if int64(i) > maxEntries {
					break
				}
				list = append(list, f.Name())
			}
			entries := strings.Join(list, ",")
			return []byte(entries), nil
		}

		// is file
		fp, err := os.Open(path)
		defer fp.Close()
		if err != nil {
			return []byte{}, errors.New("Could not open file:" + path)
		}

		// if length is < 0 then read whole file
		if length < 0 {
			finfo, err := fp.Stat()
			if err != nil {
				return []byte{}, errors.New(fmt.Sprintf("Could not get info about file:%v\n", err))
			}
			length = finfo.Size()
		}

		buf := make([]byte, length)
		if _, err := fp.ReadAt(buf, offset); err != nil {
			return []byte{}, errors.New(fmt.Sprintf("Error reading [%s]", path))
		}
		return []byte(strconv.FormatInt(int64(len(buf)), 10)), nil
	}

	service.Write = func(path string, offset, length int64, data []byte) (int64, error) {
		// create new file if it doesn't exist
		if _, err := os.Stat(path); os.IsNotExist(err) {
			if err := os.WriteFile(path, data, 0777); err != nil {
				return 0, errors.New("Could not write new file:" + path)
			}
			return int64(len(data)), nil
		}
		// othewise open file and write at offset
		trunc := 0
		if offset == 0 {
			trunc = os.O_TRUNC
		}
		f, err := os.OpenFile(path, os.O_WRONLY|trunc, 0777)
		defer f.Close()
		if err != nil {
			return 0, errors.New("Could not open file to write:" + path)
		}
		n, err := f.WriteAt(data, offset)
		if err != nil {
			return 0, errors.New(fmt.Sprintf("Could not writ to file [%s]:%v", path, err))
		}
		return int64(n), nil
	}

	service.Remove = func(path string) error {
		if err := os.Remove(path); err != nil {
			return errors.New("Could not remove file:" + err.Error())
		}
		return nil
	}

	service.Exec = func(path string, data []byte) ([]byte, error) {
		return []byte{}, errors.New("Exec operation not available")
	}

	service.Mkdir = func(path string) error {
		if err := os.MkdirAll(path, 0777); err != nil {
			return errors.New(fmt.Sprintf("Could not make directory [%s]:%s", path, err.Error()))
		}
		return nil
	}

	service.Stat = func(path string) (tro.Stat, error) {
		info, err := os.Lstat(path)
		if err != nil {
			return tro.Stat{}, errors.New("File/directory does not exist:" + path)
		}
		stat := tro.Stat{
			Name:    info.Name(),
			IsDir:   info.IsDir(),
			Size:    info.Size(),
			Updated: info.ModTime().Unix(),
		}
		return stat, nil
	}

	return service
}

func readPerms(fname string) map[string][]string {
	perms := map[string][]string{}
	fileContents, err := os.ReadFile(fname)
	if err != nil {
		os.WriteFile(fname, []byte{}, 0777)
		return perms
	}

	lines := strings.Split(string(fileContents), "\n")
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		parts := strings.Split(line, "\t")
		ps := strings.Split(parts[1], ",")
		perms[parts[0]] = ps
	}

	return perms
}

func setPerms(fname string, perms map[string][]string) error {
	s := []string{}
	for k, v := range perms {
		s = append(s, fmt.Sprintf("%s\t%s", k, strings.Join(v, ",")))
	}
	f, err := os.OpenFile(fname, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		return err
	}
	f.Write([]byte(strings.Join(s, "\n")))
	f.Close()
	return nil
}
