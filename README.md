# Netfs
A communication library built on top of Troposphere.

## Description
Netfs has three major functions. 
The first is to provide a method to expose a directory to a network.
The second is to provide a method to expose a service to a network.
The third is to provide a method to consume these network services. 
The implementations in this project use HTTP as the base protocol, with the Troposphere protocol on top. 

## Build Requirements
- Go (version 1.22.1 or greater)
- shell (optional to run the build scripts)

## Examples
Examples can be found in the `examples` directory. 

## Building Examples
Run the following script to build all the examples.
Make sure that the script file has permissions to run.
```
./build-examples.sh
```
The build script will create a `bin` directory and place all compiled examples there.

## Design
The design of netfs revolves around three concepts, the filesystem (Fs), Service, and Server.
Fs is used by clients to consume serives. 
A Server is used by servers to serve one or more services. 
A Service is a struct that implements one or more of the function signatures provided. 

This library runs the Troposphere protocol on top of the HTTP protocol.
This allows any client, that can make HTTP calls to consume serives, such as an web application.

On the client side, an Fs is used to connect to a netfs Server and provides a simplified file system interface to a service on the server.

On the server side, a provider will implement a Service and add it to a Server and then run the Server.

### Client Workflow
1. Create a new Fs
2. Connect to a Server
3. Perform operations
4. Disconnect from the Server

### Server Workflow
1. Create a new Server
2. Create and add services to the Server
3. Start the server on an address

## Security
When using Troposphere version 1.1, Troposphere messages are cryptographically hashed an signed. 
This allows the receiver to ensure that the message came from the client, and has not been tampered with. 
However, the content of the messages themselves is not encrypted by Troposphere. 
Encryption is left either to the application level, or to the underlying protocol, such as HTTPS. 

## Liscense
3-Clause BSD License
