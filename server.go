package netfs

import (
	"net/http"
)

type Server struct {
	mux *http.ServeMux
}

func NewServer() Server {
	return Server{
		mux: http.NewServeMux(),
	}
}

func (s *Server) AddService(path string, service Service) {
	s.mux.HandleFunc(path, service.Func())
}

func (s *Server) HandleFunc(path string, hf func(http.ResponseWriter, *http.Request)) {
	s.mux.HandleFunc(path, hf)
}

func (s *Server) ListenAndServe(addr string) error {
	return http.ListenAndServe(addr, s.mux)
}

func (s *Server) ListenAndServeTLS(addr, certFile, keyFile string) error {
	return http.ListenAndServeTLS(addr, certFile, keyFile, s.mux)
}
