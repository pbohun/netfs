package netfs

import (
	"bufio"
	"bytes"
	"crypto/rsa"
	"errors"
	"fmt"
	tro "gitlab.com/pbohun/troposphere"
	"log"
	"net/http"
	"strings"
)

type Fs struct {
	name      string
	addr      string
	version   string
	connected bool
	privKey   *rsa.PrivateKey
}

func NewFs(version string, privKey *rsa.PrivateKey) Fs {
	return Fs{version: version, privKey: privKey}
}

// This should be Mount and be able to mount multiple
func (fs *Fs) Connect(url string) error {
	fs.name = "/"
	fs.addr = url

	// get server version
	req, err := tro.MkMessage(tro.Tversion, fs.name, fs.privKey, 0, 0, []byte{})
	if err != nil {
		return err
	}

	resp := makeRequest(url, req)
	if string(resp.Content) != fs.version {
		return errors.New(fmt.Sprintf("Incompatible version: have[%s], want[%s]\n", string(resp.Content), fs.version))
	}

	fs.connected = true

	return nil
}

func (fs *Fs) Disconnect() {
	fs.addr = ""
	fs.connected = false
}

func (fs *Fs) Read(path string, offset, size int64) ([]byte, error) {
	if !fs.connected {
		return []byte{}, errors.New("not connected")
	}

	req, err := tro.MkMessage(tro.Tread, path, fs.privKey, offset, size, []byte{})
	if err != nil {
		return []byte{}, err
	}

	resp := makeRequest(fs.addr, req)
	if resp.KindInt() == tro.Rerror {
		return []byte{}, errors.New("error:" + string(resp.Content))
	}

	return resp.Content, nil
}

func (fs *Fs) Write(path string, offset int64, data []byte) (int64, error) {
	if !fs.connected {
		return 0, errors.New("not connected")
	}

	req, err := tro.MkMessage(tro.Twrite, path, fs.privKey, offset, int64(len(data)), data)
	if err != nil {
		return 0, err
	}

	resp := makeRequest(fs.addr, req)
	if resp.KindInt() == tro.Rerror {
		return 0, errors.New("error:" + string(resp.Content))
	}

	return resp.Length, nil
}

func (fs *Fs) Remove(path string) error {
	if !fs.connected {
		return errors.New("not connected")
	}

	req, err := tro.MkMessage(tro.Tremove, path, fs.privKey, 0, 0, []byte{})
	if err != nil {
		return err
	}

	resp := makeRequest(fs.addr, req)
	if resp.KindInt() == tro.Rerror {
		return errors.New("error:" + string(resp.Content))
	}

	return nil
}

func (fs *Fs) Exec(path string, data []byte) ([]byte, error) {
	if !fs.connected {
		return []byte{}, errors.New("not connected")
	}

	req, err := tro.MkMessage(tro.Texec, path, fs.privKey, 0, 0, data)
	if err != nil {
		return []byte{}, err
	}

	resp := makeRequest(fs.addr, req)
	if resp.KindInt() == tro.Rerror {
		return []byte{}, errors.New("error:" + string(resp.Content))
	}

	return resp.Content, nil
}

func (fs *Fs) Mkdir(path string) error {
	if !fs.connected {
		return errors.New("not connected")
	}

	req, err := tro.MkMessage(tro.Tmkdir, path, fs.privKey, 0, 0, []byte{})
	if err != nil {
		return err
	}

	resp := makeRequest(fs.addr, req)
	if resp.KindInt() == tro.Rerror {
		return errors.New("error:" + string(resp.Content))
	}

	return nil
}

func (fs *Fs) Stat(path string) (tro.Stat, error) {
	if !fs.connected {
		return tro.Stat{}, errors.New("not connected")
	}

	req, err := tro.MkMessage(tro.Tstat, path, fs.privKey, 0, 0, []byte{})
	if err != nil {
		return tro.Stat{}, err
	}

	resp := makeRequest(fs.addr, req)
	if resp.KindInt() == tro.Rerror {
		return tro.Stat{}, errors.New("error:" + string(resp.Content))
	}

	return tro.ReadStat(bufio.NewReader(bytes.NewReader(resp.Content)))
}

// Utility functions
func makeRequest(url string, msg tro.Message) tro.Message {
	resp, err := http.Post(url, "text/plain", strings.NewReader(msg.String()))
	if err != nil {
		log.Fatal("makeRequest: Could not post:", err)
	}
	rmsg, err := tro.ReadMessage(bufio.NewReader(resp.Body), 4096)
	if err != nil {
		log.Fatal("makeRequest: Could not read message:", err)
	}
	return rmsg
}
