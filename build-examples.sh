#!/usr/bin/sh
mkdir bin

go build -o bin/01-servedir/client examples/01-servedir/client.go
go build -o bin/01-servedir/server examples/01-servedir/server.go

go build -o bin/02-time/client examples/02-time/client.go
go build -o bin/02-time/server examples/02-time/server.go

go build -o bin/03-add/client examples/03-add/client.go
go build -o bin/03-add/server examples/03-add/server.go

